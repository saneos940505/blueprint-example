import * as React from 'react';
import {connect} from "react-redux";
import {push} from "react-router-redux";

namespace Header {

  export interface Props {
    goHome(): void
    goMessages(): void
  }

  export interface State {}
}

class Header extends React.Component<Header.Props, Header.State> {

  constructor(props: Header.Props) {
    super(props);

    this.goHome = this.goHome.bind(this);
    this.goMessages = this.goMessages.bind(this);
  }

  goHome() {
    this.props.goHome();
  }

  goMessages() {
    this.props.goMessages();
  }

  render() {
    return (
      <header>
        <nav className="pt-navbar pt-dark">
          <div className="pt-navbar-group pt-align-left">
            <div className="pt-navbar-heading">Blueprint Example</div>
            <input className="pt-input" placeholder="Search files..." type="text" />
          </div>
          <div className="pt-navbar-group pt-align-right">
            <button onClick={this.goMessages} className="pt-button pt-minimal pt-icon-heat-grid">Messages</button>
            <button onClick={this.goHome} className="pt-button pt-minimal pt-icon-home">Home</button>
            <span className="pt-navbar-divider"></span>
            <button className="pt-button pt-minimal pt-icon-user"></button>
            <button className="pt-button pt-minimal pt-icon-notifications"></button>
            <button className="pt-button pt-minimal pt-icon-cog"></button>
          </div>
        </nav>
      </header>
    )
  }
}

function mapDispatchToProps(dispatch: any) {
  return {
    goHome: () => dispatch(push("/home")),
    goMessages: () => dispatch(push("/messages"))
  };
}

export default connect(() => {}, mapDispatchToProps)(Header)
