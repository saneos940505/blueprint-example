import * as React from 'react';
import * as classNames from 'classnames';
import * as Classes from '@blueprintjs/core/src/common/classes';
import {Spinner} from '@blueprintjs/core';

namespace Greeting {

  export interface Props {
    message: string;
  }

  export interface State {}
}

export class Greeting extends React.Component<Greeting.Props, Greeting.State> {

  render() {
    return (
      <div>
        <h1>{this.props.message}</h1>
        <Spinner className={classNames(Classes.INTENT_PRIMARY, Classes.SMALL)} />
      </div>
    )
  }
}