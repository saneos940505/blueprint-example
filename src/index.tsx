/// <reference path="../types/app.d.ts" />

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {routerMiddleware} from 'react-router-redux';
import createBrowserHistory from 'history/createBrowserHistory';
import {configureStore} from './store/index'
import {applyMiddleware} from 'redux';
import {App} from './containers/App';

const history = createBrowserHistory();
const historyRouterMiddleware = routerMiddleware(history);
const store = configureStore(applyMiddleware(historyRouterMiddleware));

ReactDOM.render(
  <App store={store} history={history} />,
  document.getElementById('app')
);
