import * as React from 'react';
import * as Classes from '@blueprintjs/core/src/common/classes';
import * as style from './style.scss';
import * as classNames from 'classnames';
import {RouteComponentProps} from "react-router";
import {Helmet} from "react-helmet";
import {Popover} from "@blueprintjs/core";

namespace MessageBeard {

  export interface Props extends RouteComponentProps<void> {}

  export interface State {}
}

export class MessageBoard extends React.Component<MessageBeard.Props, MessageBeard.State> {

  get content() {
    return (
      <div>
        <h5>Hello</h5>
        <button className={classNames(Classes.BUTTON, Classes.POPOVER_DISMISS)}>Dismiss</button>
      </div>
    );
  }

  render() {
    const baseCN = [Classes.CARD, Classes.ELEVATION_1, Classes.INTERACTIVE, style.card];

    return (
      <div className={style.messageBoard}>
        <Helmet>
          <title>Message Board</title>
        </Helmet>
        <Popover>
          <div className={classNames(baseCN, style.weirdBg)} />
          {this.content}
        </Popover>
        <Popover content={this.content}>
          <div className={classNames(baseCN, style.sleepBg)} />
        </Popover>
      </div>
    );
  }
}