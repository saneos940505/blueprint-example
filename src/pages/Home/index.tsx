import * as React from 'react';
import {Greeting} from '../../components/Greeting';
import {RouteComponentProps} from "react-router";
import {Helmet} from "react-helmet";

namespace Home {

  export interface Props extends RouteComponentProps<void> {}

  export interface State {}
}

export class Home extends React.Component<Home.Props, Home.State> {

  render() {
    return (
      <div>
        <Helmet>
          <title>Home</title>
        </Helmet>
        <Greeting message="Hello, world." />
      </div>
    )
  }
}