import {createStore} from 'redux';
import rootReducer from '../reducers';

export const configureStore = (initialState: any) => createStore(rootReducer, initialState);
