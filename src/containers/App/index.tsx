import * as React from 'react';
import {Provider} from 'react-redux';
import {Route} from 'react-router';
import {ConnectedRouter, ConnectedRouterProps} from 'react-router-redux';
import {Home} from "../../pages/Home/index";
import {MessageBoard} from "../../pages/MessageBoard/index";
import {Layout} from "../Layout/index";

namespace App {

  export interface Props extends ConnectedRouterProps<AppState> {}

  export interface State {}
}

export class App extends React.Component<App.Props, App.State> {

  render() {
    const { store, history } = this.props;

    return (
      <Provider store={store}>
        <Layout>
          <ConnectedRouter history={history}>
            <div>
              <Route path="/home" component={Home} />
              <Route path="/messages" component={MessageBoard} />
            </div>
          </ConnectedRouter>
        </Layout>
      </Provider>
    );
  }
}