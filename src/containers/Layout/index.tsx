import * as React from 'react';
import Header from "../../components/Header";

namespace Layout {

  export interface Props {}

  export interface State {}
}

export class Layout extends React.Component<Layout.Props, Layout.State> {

  render() {
    return (
      <div>
        <Header />
        {this.props.children}
      </div>
    );
  }
}