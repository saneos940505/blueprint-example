# Blueprint Example

A front-end web application with Blueprint UI components. This project inspects whether Blueprint is suitable for developing Live'n Tokyo client.

Blueprint is documented here: http://blueprintjs.com/docs/

## Requirements

You need to install`webpack` and `webpack-dev-server` on your system.

## Setup

To setup and run this app just execute the following commands:

```
$ yarn
$ yarn serve
```

## TODO
 - Fix a bug a popover would not dismiss.
 - Try to use reselect for react-redux.
 - Implement a load progress bar on the top of page.
 
## Reference
 These links are not related to Blueprint.
 * [Programmatically navigate using react router](https://stackoverflow.com/questions/31079081/programmatically-navigate-using-react-router)
 * [Redux Docs](http://redux.js.org/)
 * [TypeScript with React & Webpack](https://www.typescriptlang.org/docs/handbook/react-&-webpack.html)
 * [rokoroku/react-redux-typescript-boilerplate](https://github.com/rokoroku/react-redux-typescript-boilerplate)
