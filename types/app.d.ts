declare type AppState = {
  message: string;
}

declare module '*.scss' {
  export const card: any;
  export const messageBoard: any;
  export const weirdBg: any;
  export const sleepBg: any;
}